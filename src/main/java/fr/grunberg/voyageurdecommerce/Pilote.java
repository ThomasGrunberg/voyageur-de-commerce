package fr.grunberg.voyageurdecommerce;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import fr.grunberg.voyageurdecommerce.algorithmes.Algorithme;
import fr.grunberg.voyageurdecommerce.algorithmes.AlgorithmeGénétique;
import fr.grunberg.voyageurdecommerce.algorithmes.AlgorithmeGéométrique;
import fr.grunberg.voyageurdecommerce.algorithmes.Optimiseur;
import fr.grunberg.voyageurdecommerce.donnees.Données;
import fr.grunberg.voyageurdecommerce.ihm.FenêtrePrincipale;

public class Pilote implements Runnable {
	private final FenêtrePrincipale ihm;
	private final Données données;
	private final Optimiseur optimiseur;
	private final long tempsDébut;
	private boolean enPause;
	private boolean enService;
	
	private Algorithme algorithmeGéométrique;
	private Algorithme algorithmeGénétique;
	private boolean algorithmeGéométriqueActivé;
	private boolean algorithmeGénétiqueActivé;
	private List<Algorithme> algorithmesAutorisés;
	
	public Pilote() {
		this.algorithmeGéométriqueActivé = true;
		this.algorithmeGénétiqueActivé = true;
		this.tempsDébut = System.currentTimeMillis();
		this.ihm = new FenêtrePrincipale(this);
		this.données = new Données();
		this.algorithmesAutorisés = new ArrayList<>(2);
		this.algorithmeGéométrique = new AlgorithmeGéométrique(données);
		this.algorithmeGénétique = new AlgorithmeGénétique(données, 1);
		recalculerAlgorithmesAutorisés();
		this.optimiseur = new Optimiseur(this, algorithmeGéométrique);
		this.ihm.mettreAJour(this.optimiseur.getMeilleurTrajetActuel());
		this.enPause = true;
		this.enService = true;
		new Thread(this).start();
		new Thread(this.optimiseur).start();
	}

	public Données getDonnées() {
		return données;
	}

	public long getTempsDébut() {
		return tempsDébut;
	}

	@Override
	public void run() {
		while(enService) {
			if(this.optimiseur.aAmélioréTrajet())
				this.ihm.mettreAJour(this.optimiseur.getMeilleurTrajetActuel());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}
	}

	public Algorithme choisirAlgorithme(Algorithme algorithmePrécédementChoisi) {
		if(algorithmesAutorisés.size() == 1)
			return algorithmesAutorisés.get(0);
		Collections.shuffle(algorithmesAutorisés);
		return algorithmesAutorisés.stream()
				.filter(algo -> algo != algorithmePrécédementChoisi)
				.findFirst()
				.get();
	}

	private void recalculerAlgorithmesAutorisés() {
		List<Algorithme> algorithmesAutorisésMaintenant = new ArrayList<>(2);
		if(algorithmeGéométriqueActivé)
			algorithmesAutorisésMaintenant.add(algorithmeGéométrique);
		if(algorithmeGénétiqueActivé)
			algorithmesAutorisésMaintenant.add(algorithmeGénétique);
		if(algorithmesAutorisésMaintenant.isEmpty())
			algorithmesAutorisésMaintenant.add(algorithmeGéométrique);
		this.algorithmesAutorisés = algorithmesAutorisésMaintenant;
	}
	
	public void enPause(boolean enPause) {
		this.enPause = enPause;
	}

	public void activerAlgorithmeGéométrique(boolean activer) {
		algorithmeGéométriqueActivé = activer;
		recalculerAlgorithmesAutorisés();
	}

	public boolean estAlgorithmeGéométriqueActivé() {
		return algorithmeGéométriqueActivé;
	}

	public void activerAlgorithmeGénétique(boolean activer) {
		algorithmeGénétiqueActivé = activer;
		recalculerAlgorithmesAutorisés();
	}

	public boolean estAlgorithmeGénétiqueActivé() {
		return algorithmeGénétiqueActivé;
	}

	public boolean estEnPause() {
		return this.enPause ;
	}

	public boolean estEnService() {
		return enService;
	}
}
