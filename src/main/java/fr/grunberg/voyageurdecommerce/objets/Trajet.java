package fr.grunberg.voyageurdecommerce.objets;

import java.lang.reflect.MalformedParametersException;
import java.util.List;
import java.util.stream.Collectors;

import fr.grunberg.voyageurdecommerce.donnees.Données;

public final class Trajet implements Comparable<Trajet> {
	private final List<Ville> villes;
	private final double longueur;
	
	public Trajet(List<Ville> villes) {
		if(Données.NOMBRE_DE_VILLES != villes.size())
			throw new MalformedParametersException("Le nombre de villes fournies ne correspond pas à l'attendu : " + villes.size() + " !=" + Données.NOMBRE_DE_VILLES);
		this.villes = villes.stream().collect(Collectors.toUnmodifiableList());
		double longueurCalculée = 0;
		for(int i = 1; i < villes.size(); i++)
			longueurCalculée += villes.get(i).getDistance(villes.get(i-1));
		longueur = longueurCalculée;
	}

	public List<Ville> getVilles() {
		return villes;
	}
	
	public double getLongueur() {
		return longueur;
	}
	
	@Override
	public String toString() {
		return longueur + " " + longueur + " ; " + getDétails();
	}

	public String getDétails() {
		return villes.stream().map(v -> String.valueOf(v.getNuméro())).collect(Collectors.joining(", ", "[", "]"));
		}

	@Override
	public int compareTo(Trajet autreTrajet) {
		if(longueur ==  autreTrajet.getLongueur())
			return 0;
		if(longueur <  autreTrajet.getLongueur())
			return -1;
		return 1;
	}
}
