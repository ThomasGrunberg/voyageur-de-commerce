package fr.grunberg.voyageurdecommerce.objets;

import java.util.List;

public final class Ville {
	public boolean Libre;
	public int Voisins[];
	public int NombreDeVoisins;
	private double distances[];

	public final int numéro;
	private final double coordonnéeX;
	private final double coordonnéeY;

	public Ville(int numéro, double coordonnéeX, double coordonnéeY) {
		this.coordonnéeX = coordonnéeX;
		this.coordonnéeY = coordonnéeY;
		this.numéro = numéro;
		Libre = true;
		distances = new double[250];
		int i;
		for (i = 0; i < 250; i++)
			distances[i] = -1;
	}

	protected double getDistance(double x, double y) {
		return Math.sqrt((x - coordonnéeX) * (x - coordonnéeX) + (y - coordonnéeY) * (y - coordonnéeY));
	}

	private double getDistance(int NumeroVoisin, Ville voisin) {
		if (distances[NumeroVoisin] < 0) {
			distances[NumeroVoisin] = voisin.getDistance(coordonnéeX, coordonnéeY);
		}
		return distances[NumeroVoisin];
	}

	public double getDistance(Ville voisin) {
		return getDistance(voisin.getNuméro(), voisin);
	}

	public double getCoordonnéeX() {
		return coordonnéeX;
	}

	public double getCoordonnéeY() {
		return coordonnéeY;
	}

	public int getNuméro() {
		return numéro;
	}

	public Ville getMeilleurVoisin(List<Ville> voisinsAEtudier) {
		if (voisinsAEtudier == null || voisinsAEtudier.isEmpty()) {
			throw new ArrayIndexOutOfBoundsException("Pas de voisin disponible à planifier");
		}
		if(voisinsAEtudier.size() == 1)
			return voisinsAEtudier.get(0);
		double plusPetitDistanceTrouvée = Double.MAX_VALUE;
		Ville meilleurVoisinTrouvé = null;
		for(Ville voisin : voisinsAEtudier) {
			double distanceVoisin = this.getDistance(voisin);
			if(distanceVoisin < plusPetitDistanceTrouvée) {
				plusPetitDistanceTrouvée = distanceVoisin;
				meilleurVoisinTrouvé = voisin;
			}
		}
		return meilleurVoisinTrouvé;
	}
}
