package fr.grunberg.voyageurdecommerce.ihm;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.EventObject;

import fr.grunberg.voyageurdecommerce.Pilote;
import fr.grunberg.voyageurdecommerce.donnees.Données;
import fr.grunberg.voyageurdecommerce.objets.Trajet;
import fr.grunberg.voyageurdecommerce.objets.Ville;

public class FenêtrePrincipale extends Frame implements ActionListener, ItemListener {
	private static final long serialVersionUID = 1L;
	private final int XMin = 350, XMax = 750, YMin = 50, YMax = 450;
	
	private static final Color couleurTrajetActuel = Color.blue;
	private static final Color couleurTrajetPrécédent = Color.green;

	private final Pilote pilote;
	
	private Trajet trajetActuel;
	private Trajet trajetPrécédent;
	private Label Rouge, Vert, Bleu;
	private boolean dessinerPrécédentTrajet = false;
	private String étatAlgo;
	private String étatTemps;
	private String étatLongueur;

	private Button boutonDémarrageEtPause;
	private Checkbox caseàCocherAlgoGéométrique, caseàCocherAlgoGénétique;
	private Label libelléStatutAlgo, libelléStatutTemps, libelléStatutLongueur, libelléEntêteChoixAlgorithmes;
	private TextArea affichageTrajet;

	public FenêtrePrincipale(Pilote pilote) {
		super();
		this.pilote = pilote;
		Rouge = new Label("Ville");
		Bleu = new Label("Trajet optimal actuel");
		if (dessinerPrécédentTrajet)
			Vert = new Label("Dernières Modifications");
		Rouge.setBounds(XMin + 150, YMax + 10, 200, 30);
		Bleu.setBounds(XMin + 150, YMax + 40, 200, 30);
		if (dessinerPrécédentTrajet)
			Vert.setBounds(XMin + 150, YMax + 70, 200, 30);
		add(Rouge);
		add(Bleu);
		if (dessinerPrécédentTrajet)
			add(Vert);
		
		setBounds(0,0,800,600);
		setLayout(null);

		libelléStatutAlgo = new Label();
		libelléStatutTemps = new Label();
		libelléStatutLongueur = new Label();
		libelléEntêteChoixAlgorithmes = new Label("Utiliser :");
		affichageTrajet = new TextArea();
		boutonDémarrageEtPause = new Button("Démarrer");
		caseàCocherAlgoGéométrique = new Checkbox("Algorithme Géométrique", pilote.estAlgorithmeGéométriqueActivé());
		caseàCocherAlgoGéométrique.addItemListener(this);
		caseàCocherAlgoGénétique = new Checkbox("Algorithme Génétique", pilote.estAlgorithmeGénétiqueActivé());
		caseàCocherAlgoGénétique.addItemListener(this);
		
		affichageTrajet.setBounds(10,300,300,50);
		libelléStatutAlgo.setBounds(10,80,300,50);
		add(libelléStatutAlgo);
		libelléStatutTemps.setBounds(10,120,300,50);
		add(libelléStatutTemps);
		libelléStatutLongueur.setBounds(10,160,300,50);
		add(libelléStatutLongueur);
		boutonDémarrageEtPause.setBounds(250,470,100,30);
		add(boutonDémarrageEtPause);
		boutonDémarrageEtPause.addActionListener(this);

		libelléEntêteChoixAlgorithmes.setBounds(10, 440, 200, 30);
		add(libelléEntêteChoixAlgorithmes);
		caseàCocherAlgoGéométrique.setBounds(10,470,200,30);
		add(caseàCocherAlgoGéométrique);
		caseàCocherAlgoGénétique.setBounds(10,500,200,30);
		add(caseàCocherAlgoGénétique);

		setVisible(true);
	}

	private void setTrajet(Trajet trajet) {
		trajetPrécédent = trajetActuel;
		trajetActuel = trajet;
		if(trajetPrécédent == null)
			trajetPrécédent = trajet;
	}

	public void paint(Graphics g) {
		int i;
		if (trajetActuel != null) {
			for (i = 0; i < Données.NOMBRE_DE_VILLES - 1; i++) {
				dessineVille(trajetActuel.getVilles().get(i), g);
				if (dessinerPrécédentTrajet)
					if ((trajetPrécédent.getVilles().get(i) != trajetActuel.getVilles().get(i)) 
							|| (trajetPrécédent.getVilles().get(i+1) != trajetActuel.getVilles().get(i+1)))
						dessineLigne(trajetPrécédent.getVilles().get(i), trajetPrécédent.getVilles().get(i+1), g, couleurTrajetPrécédent);
				dessineLigne(trajetActuel.getVilles().get(i), trajetActuel.getVilles().get(i+1), g, couleurTrajetActuel);
			}
			dessineLigne(trajetActuel.getVilles().get(i), trajetActuel.getVilles().get(0), g, Color.blue);
			dessineVille(trajetActuel.getVilles().get(i), g);
			TraceLegende(g);
		}
	}

	private void dessineVille(Ville ville, Graphics g) {
		g.setColor(Color.red);
		g.drawRect((int) (ville.getCoordonnéeX() * (XMax - XMin)) + XMin,
				(int) (ville.getCoordonnéeY() * (YMax - YMin)) + YMin, 2, 2);
	}

	private void dessineLigne(Ville départ, Ville arrivée, Graphics g, Color couleur) {
		g.setColor(couleur);
		g.drawLine((int) (départ.getCoordonnéeX() * (XMax - XMin)) + XMin,
				(int) (départ.getCoordonnéeY() * (YMax - YMin)) + YMin,
				(int) (arrivée.getCoordonnéeX() * (XMax - XMin)) + XMin,
				(int) (arrivée.getCoordonnéeY() * (YMax - YMin)) + YMin);
	}

	private void TraceLegende(Graphics g) {
		g.setColor(Color.red);
		g.drawRect(XMin + 130, YMax + 25, 2, 2);
		g.setColor(Color.blue);
		g.drawLine(XMin + 110, YMax + 55, XMin + 135, YMax + 55);
		if (dessinerPrécédentTrajet) {
			g.setColor(Color.green);
			g.drawLine(XMin + 110, YMax + 85, XMin + 135, YMax + 85);
		}
	}

	private void traiterEvénement(EventObject e) {
		if (e.getSource() == boutonDémarrageEtPause) {
			pilote.enPause(!pilote.estEnPause());
			add(affichageTrajet);
			if (pilote.estEnPause()) {
				boutonDémarrageEtPause.setLabel("Reprendre");
				System.out.println("En Pause");
			} else {
				boutonDémarrageEtPause.setLabel("Pause");
			}
		} else if (e.getSource() == this.caseàCocherAlgoGéométrique) {
			pilote.activerAlgorithmeGéométrique(caseàCocherAlgoGéométrique.getState());
		} else if (e.getSource() == this.caseàCocherAlgoGénétique) {
			pilote.activerAlgorithmeGénétique(caseàCocherAlgoGénétique.getState());
		}
		mettreAJour(null);
	}

	public void mettreAJour(Trajet nouveauTrajet) {
		if(nouveauTrajet != null) {
			setTrajet(nouveauTrajet);
			System.out.println(getTemps() + " : Longueur : " + Double.toString(trajetActuel.getLongueur()) + " ; détails : " + trajetActuel.getDétails());
			affichageTrajet.setText(trajetActuel.getDétails());
			libelléStatutLongueur.setText(étatLongueur);
		}
		libelléStatutAlgo.setText(étatAlgo);
		libelléStatutTemps.setText(étatTemps);
		repaint();
	}

	private String getTemps() {
		int Secondes = 0, Minutes = 0, Heures = 0;
		String s = "";
		Secondes = (int)((System.currentTimeMillis() - pilote.getTempsDébut())/1000);
		if(Secondes>60)
			{
			Minutes = (int)(Secondes/60);
			Secondes -= 60*Minutes;
			if(Minutes>60)
				{
				Heures = (int)(Minutes/60);
				Minutes -= 60*Heures;
				s = Integer.toString(Heures).concat(" Heures, ");
				}
			s = s.concat(Integer.toString(Minutes).concat(" minutes et "));
			}
		s = s.concat(Integer.toString(Secondes).concat(" secondes"));
		return s;
		}

	@Override
	public void actionPerformed(ActionEvent e) {
		traiterEvénement(e);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		traiterEvénement(e);
	}
}
