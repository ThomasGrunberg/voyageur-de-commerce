package fr.grunberg.voyageurdecommerce.algorithmes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.grunberg.voyageurdecommerce.donnees.Données;
import fr.grunberg.voyageurdecommerce.objets.Trajet;
import fr.grunberg.voyageurdecommerce.objets.Ville;

public class AlgorithmeGéométrique extends Algorithme {
	public AlgorithmeGéométrique(Données données) {
		super(données);
	}

	@Override
	public Trajet calculerTrajetInitial() {
		List<Ville> villesPlanifiées = new ArrayList<>(Données.NOMBRE_DE_VILLES);
		villesPlanifiées.add(données.getVille(0));
		List<Ville> villesNonPlanifiées = données.getVilles();
		Ville dernièreVillePlanifiée = données.getVille(0);
		villesNonPlanifiées.remove(dernièreVillePlanifiée);
		while(!villesNonPlanifiées.isEmpty()) {
			Ville prochaineVillePlanifiée = dernièreVillePlanifiée.getMeilleurVoisin(villesNonPlanifiées);
			villesPlanifiées.add(prochaineVillePlanifiée);
			villesNonPlanifiées.remove(prochaineVillePlanifiée);
			dernièreVillePlanifiée = prochaineVillePlanifiée;
		}
		return new Trajet(villesPlanifiées);
	}
	
	@Override
	public Optional<Trajet> améliorerTrajet(Trajet trajetAAméliorer) {
		Ville villeAvecPiresDistances = trouverVilleAvecPiresDistances(trajetAAméliorer);
		int indiceCible = calculerMeilleureNouvellePosition(trajetAAméliorer, villeAvecPiresDistances);
		Trajet nouveauTrajet = new Trajet(déplacerVille(trajetAAméliorer, villeAvecPiresDistances, indiceCible));
		if(nouveauTrajet.getLongueur() < trajetAAméliorer.getLongueur())
			return Optional.of(nouveauTrajet);
		else
			return Optional.empty();
	}

	private Ville trouverVilleAvecPiresDistances(Trajet trajetAAméliorer) {
		double pireDistanceTrouvée = 0d;
		Ville villeAvecPireDistance = null;
		for(int i = 1; i<trajetAAméliorer.getVilles().size(); i++) {
			double distanceActuelle = trajetAAméliorer.getVilles().get(i).getDistance(trajetAAméliorer.getVilles().get(i-1));
			if(i == trajetAAméliorer.getVilles().size()-1)
				distanceActuelle += trajetAAméliorer.getVilles().get(i).getDistance(trajetAAméliorer.getVilles().get(0));
			else
				distanceActuelle += trajetAAméliorer.getVilles().get(i).getDistance(trajetAAméliorer.getVilles().get(i+1));
			if(distanceActuelle > pireDistanceTrouvée) {
				villeAvecPireDistance = trajetAAméliorer.getVilles().get(i);
				pireDistanceTrouvée = distanceActuelle;
			}
		}
		return villeAvecPireDistance;
	}

	
	@Override
	public String toString() {
		return "Algorithme géométrique";
	}
}
