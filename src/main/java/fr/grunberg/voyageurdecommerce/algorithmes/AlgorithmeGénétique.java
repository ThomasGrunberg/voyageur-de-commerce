package fr.grunberg.voyageurdecommerce.algorithmes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import fr.grunberg.voyageurdecommerce.donnees.Données;
import fr.grunberg.voyageurdecommerce.objets.Trajet;
import fr.grunberg.voyageurdecommerce.objets.Ville;

public class AlgorithmeGénétique extends Algorithme {
	private static final int NOMBRE_MUTATIONS_MIN = 0;
	private static final int NOMBRE_MUTATIONS_MAX = 3;
	private static final int GENERATIONS = 5;
	private static final int POPULATION = 20;
	private static final int POPULATION_SELECTIONNEE = 5;

	private final long graineAléatoire;
	private final Random générateurAléatoire;

	public AlgorithmeGénétique(Données données, long graineAléatoire) {
		super(données);
		this.graineAléatoire = graineAléatoire;
		this.générateurAléatoire = new Random(graineAléatoire);
	}

	@Override
	public Optional<Trajet> améliorerTrajet(Trajet trajetAAméliorer) {
		List<Trajet> trajetsMutés = List.of(trajetAAméliorer);
		for(int c = 0; c < GENERATIONS; c++) {
			trajetsMutés = reproduireTrajets(trajetsMutés.subList(0, Math.min(1, POPULATION_SELECTIONNEE)))
					.parallelStream()
					.map(t -> muterTrajet(t))
					.sorted()
					.toList();
		}
		if(trajetsMutés.get(0).getLongueur() < trajetAAméliorer.getLongueur())
			return Optional.of(trajetsMutés.get(0));
		return Optional.empty();
	}

	private List<Trajet> reproduireTrajets(List<Trajet> trajetsInitiaux) {
		if(trajetsInitiaux.size() == POPULATION)
			return trajetsInitiaux;
		List<Trajet> trajetsReproduits = new ArrayList<>(POPULATION);
		int i = 0;
		while(trajetsReproduits.size() < POPULATION) {
			trajetsReproduits.add(trajetsInitiaux.get(i));
			i++;
			if(i >= trajetsInitiaux.size())
				i = 0;
		}
		return trajetsReproduits;
	}
	
	private Trajet muterTrajet(Trajet trajetàMuter) {
		int nombreMutations = générateurAléatoire.nextInt(NOMBRE_MUTATIONS_MIN, NOMBRE_MUTATIONS_MAX);
		List<Ville> villesTrajetàMuter = trajetàMuter.getVilles()
				.stream()
				.collect(Collectors.toList());

		for(int m = 0; m < nombreMutations; m++) {
			TypeMutation typeMutation = TypeMutation.getMutationAléatoire(générateurAléatoire);
			switch(typeMutation) {
				case SUBSTITUTION :
					villesTrajetàMuter = appliquerSubstitution(villesTrajetàMuter);
					break;
				case DEPLACEMENT_DE_BRANCHE :
					villesTrajetàMuter = appliquerDéplacementDeBranche(villesTrajetàMuter);
					break;
				case INVERSION_DE_BRANCHE :
					villesTrajetàMuter = appliquerInversionDeBranche(villesTrajetàMuter);
					break;
			}
		}
		return new Trajet(villesTrajetàMuter);
	}

	private List<Ville> appliquerInversionDeBranche(List<Ville> villesTrajetàMuter) {
		int indiceBrancheDépart = générateurAléatoire.nextInt(0, Données.NOMBRE_DE_VILLES);
		int indiceBrancheArrivée = indiceBrancheDépart + générateurAléatoire.nextInt(1, Données.NOMBRE_DE_VILLES/5);
		if(indiceBrancheArrivée >= Données.NOMBRE_DE_VILLES)
			indiceBrancheArrivée = Données.NOMBRE_DE_VILLES -1;
		List<Ville> branche = villesTrajetàMuter.subList(indiceBrancheDépart, indiceBrancheArrivée).stream().collect(Collectors.toList());
		villesTrajetàMuter.removeAll(branche);
		while(!branche.isEmpty())
			villesTrajetàMuter.add(indiceBrancheDépart, branche.remove(0));
		return villesTrajetàMuter;
	}
	
	private List<Ville> appliquerDéplacementDeBranche(List<Ville> villesTrajetàMuter) {
		int indiceBrancheDépart = générateurAléatoire.nextInt(0, Données.NOMBRE_DE_VILLES-1);
		int indiceBrancheArrivée = indiceBrancheDépart + générateurAléatoire.nextInt(1, Données.NOMBRE_DE_VILLES/5);
		if(indiceBrancheArrivée >= Données.NOMBRE_DE_VILLES)
			indiceBrancheArrivée = Données.NOMBRE_DE_VILLES -1;
		int indiceCible = générateurAléatoire.nextInt(0, Données.NOMBRE_DE_VILLES);
		List<Ville> branche = villesTrajetàMuter.subList(indiceBrancheDépart, indiceBrancheArrivée).stream().collect(Collectors.toList());
		villesTrajetàMuter.removeAll(branche);
		if(indiceCible >= villesTrajetàMuter.size())
			indiceCible = villesTrajetàMuter.size()-1;
		while(!branche.isEmpty())
			villesTrajetàMuter.add(indiceCible, branche.remove(branche.size()-1));
		return villesTrajetàMuter;
	}
	
	private List<Ville> appliquerSubstitution(List<Ville> villesTrajetàMuter) {
		Ville villeàDéplacer = villesTrajetàMuter.get(générateurAléatoire.nextInt(0, Données.NOMBRE_DE_VILLES));
		int indiceCible = générateurAléatoire.nextInt(0, Données.NOMBRE_DE_VILLES);
		return déplacerVille(villesTrajetàMuter, villeàDéplacer, indiceCible);
	}

	public long getGraineAléatoire() {
		return graineAléatoire;
	}
	
	@Override
	public String toString() {
		return "Algorithme génétique";
	}
}
