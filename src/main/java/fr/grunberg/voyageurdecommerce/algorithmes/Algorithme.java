package fr.grunberg.voyageurdecommerce.algorithmes;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fr.grunberg.voyageurdecommerce.donnees.Données;
import fr.grunberg.voyageurdecommerce.objets.Trajet;
import fr.grunberg.voyageurdecommerce.objets.Ville;

public abstract class Algorithme {
	protected final Données données;
	
	protected Algorithme(Données données) {
		this.données = données;
	}
	
	protected final List<Ville> déplacerVille(Trajet trajet, Ville villeàDéplacer, int indiceCible) {
		return déplacerVille(trajet.getVilles(), villeàDéplacer, indiceCible);
	}
	protected final List<Ville> déplacerVille(List<Ville> villes, Ville villeàDéplacer, int indiceCible) {
		List<Ville> nouveauTrajet = villes.stream().collect(Collectors.toList());
		nouveauTrajet.remove(villeàDéplacer);
		nouveauTrajet.add(indiceCible, villeàDéplacer);
		return nouveauTrajet;
	}
	protected final int calculerMeilleureNouvellePosition(Trajet trajetAAméliorer, Ville villeàDéplacer) {
		double meilleureDistanceTrouvée = Double.MAX_VALUE;
		int meilleurePositionTrouvée = -1;
		for(int i = 1; i<trajetAAméliorer.getVilles().size(); i++) {
			double nouvelleDistance = 
					villeàDéplacer.getDistance(trajetAAméliorer.getVilles().get(i-1))
					+ villeàDéplacer.getDistance(trajetAAméliorer.getVilles().get(i));
			if(nouvelleDistance < meilleureDistanceTrouvée) {
				meilleureDistanceTrouvée = nouvelleDistance;
				meilleurePositionTrouvée = i;
			}
		}
		return meilleurePositionTrouvée;
	}

	public Trajet calculerTrajetInitial() {
		return new Trajet(
				IntStream.range(0, Données.NOMBRE_DE_VILLES)
					.mapToObj(n -> données.getVille(n))
					.collect(Collectors.toList())
				);
	}

	public abstract Optional<Trajet> améliorerTrajet(Trajet trajetAAméliorer);

}
