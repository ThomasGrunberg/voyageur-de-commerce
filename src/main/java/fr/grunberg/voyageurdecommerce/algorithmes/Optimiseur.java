package fr.grunberg.voyageurdecommerce.algorithmes;

import java.util.Optional;

import fr.grunberg.voyageurdecommerce.Pilote;
import fr.grunberg.voyageurdecommerce.objets.Trajet;

public final class Optimiseur implements Runnable {
	private final Pilote pilote;
	private Trajet meilleurTrajetActuel;
	private boolean aAmélioréTrajet;

	public Optimiseur(Pilote pilote, Algorithme algorithmeInitialisation) {
		this.pilote = pilote;
		this.meilleurTrajetActuel = algorithmeInitialisation.calculerTrajetInitial();
	}
	
	@Override
	public void run() {
		Algorithme algorithmeChoisi = null;
		while(pilote.estEnService()) {
			if(pilote.estEnPause()) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
			else {
				algorithmeChoisi = pilote.choisirAlgorithme(algorithmeChoisi);
				Optional<Trajet> nouveauMeilleurTrajet = algorithmeChoisi.améliorerTrajet(meilleurTrajetActuel);
				if(nouveauMeilleurTrajet.isPresent()) {
					System.out.println("Trajet amélioré par " + algorithmeChoisi.toString() + " de " + meilleurTrajetActuel.getLongueur() + " à " + nouveauMeilleurTrajet.get().getLongueur());
					meilleurTrajetActuel = nouveauMeilleurTrajet.get();
					aAmélioréTrajet = true;
				}
			}
		}
	}
	
	public Trajet getMeilleurTrajetActuel() {
		return meilleurTrajetActuel;
	}

	public boolean aAmélioréTrajet() {
		if(aAmélioréTrajet) {
			aAmélioréTrajet = false;
			return true;
		}
		else {
			return false;
		}
	}
}
