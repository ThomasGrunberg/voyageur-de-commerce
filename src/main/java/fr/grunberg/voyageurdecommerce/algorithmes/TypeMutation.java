package fr.grunberg.voyageurdecommerce.algorithmes;

import java.util.Random;

public enum TypeMutation {
	SUBSTITUTION,
	INVERSION_DE_BRANCHE,
	DEPLACEMENT_DE_BRANCHE;

	public static TypeMutation getMutationAléatoire(Random générateurAléatoire) {
		switch(générateurAléatoire.nextInt(1, 3+1)) {
			case 1 : return SUBSTITUTION;
			case 2 : return DEPLACEMENT_DE_BRANCHE;
			case 3 : return INVERSION_DE_BRANCHE;
			default : throw new ArrayIndexOutOfBoundsException("Valeur aléatoire non permise.");
		}
	}
}
