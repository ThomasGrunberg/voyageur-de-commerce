package fr.grunberg.voyageurdecommerce.donnees;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class DonnéesTest {
	@Test
	public void tailleDonnées() {
		Données données = new Données();
		assertEquals(250, données.getCoordonnées().length);
		assertEquals(250, données.getMeilleurTrajetTrouve().length);
	}
}
