package fr.grunberg.voyageurdecommerce.objets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

public class VilleTest {
	@Test
	public void constructeur() {
		Ville ville = new Ville(0, 1d, 1d);
		assertEquals(1d, ville.getDistance(0d, 1d), 0.125d);
		assertEquals(1d, ville.getCoordonnéeX(), 0d);
		assertEquals(1d, ville.getCoordonnéeY(), 0d);
	}
	
	@Test
	public void trouverMeilleurVoisin() {
		Ville ville1 = new Ville(0, 1d, 1d);
		Ville ville2 = new Ville(0, 0.8d, 1d);
		Ville ville3 = new Ville(0, 0d, 1d);
		Ville ville4 = new Ville(0, 1d, 0d);
		Ville meilleurVoisin = ville1.getMeilleurVoisin(List.of(ville2, ville3, ville4));
		assertNotNull(meilleurVoisin);
		assertEquals(meilleurVoisin, ville2);
	}
}
