package fr.grunberg.voyageurdecommerce.objets;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import fr.grunberg.voyageurdecommerce.donnees.Données;

public class TrajetTest {
	@Test
	public void constructeur() {
		Ville villeUnique = new Ville(0, 0d, 0d);
		List<Ville> villes = 
				IntStream.range(0, Données.NOMBRE_DE_VILLES)
					.mapToObj(i -> villeUnique)
					.collect(Collectors.toList());
		Trajet trajet = new Trajet(villes);
		assertEquals(0d, trajet.getLongueur(), 0d);
	}
}
