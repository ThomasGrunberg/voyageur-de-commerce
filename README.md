# Problème du voyageur de commerce.
Le problème est simple en apparence. Il s'agit de trouver le chemin le plus court entre n villes, qui passe une seule fois par chaque ville. Un peu comme un voyageur de commerce qui parcours la France et cherche à passer le moins de temps possible sur la route.

![alt exemple de trajet](img/villes.png "exemple de trajet")

Exemple de trajet pour quelques villes françaises

Simple, semble-t-il, pour un ordinateur : il n'y a qu'à analyser toutes les solutions.
Là où le problème se complique, c'est quand le nombre de villes devient élevé. Le temps de calcul croit de manière exponentielle. A partir de 30 villes, il serait impossible de calculer bêtement toutes les possibilités. Cela prendrait ... plusieurs milliards d'années !

Le problème du voyageur de commerce est dit "NP complet", dont les possibilités augmentent exponentiellement avec le nombre de villes :
Si n est le nombre de villes, le nombre de possibilités (nombre de trajets possibles) est de (n-1)! / 2.
Le tableau suivant montre le nombre de trajets possibles, et le temps de calcul estimé pour évaluer tous ces trajets (On considère que l'on évalue un trajet en 1 microseconde - ce qui est déjà très rapide) :

| Nb de villes | Nb de possibilités | Temps de calcul |
| ----------- | ----------- | ----------- |
| 5 | 12 | 12 microsecondes |
| 10 | 181 440 | 0,18 seconde |
| 15 | 43 milliards | 12 heures |
| 20 | 60 E+15 | 1928 ans |
| 25 | 310 E+21 | 9,8 milliards d'années |

C'est ce qu'on appelle l'explosion combinatoire.
C'est le nombre de trajets de peu d'intérêt que l'on teste qui limite cet algorithme : Quand on teste toutes les possibilités, on teste même les plus absurdes.

Il faut donc trouver un autre algorithme pour résoudre le problème, qui trouverait dans un temps raisonnable une solution approchée.


Exemple de trajet pour le défi des 250 villes

Beaucoup de méthodes atteignant ce but ont été développées (dont certaines assez récement) : méthode des plus proches voisins, de Lin-Kernighan, de l'élastique, du recuit simulé, les réseaux de neurones auto-adaptatifs, algorithmes génétiques, tabu search, colonie de fourmis...
Certaines méthodes sont très simples, d'autres très complexes... Ce qui est sûr, c'est que ce problème stimule la créativité ! 

Le résultat de certains grands esprits se retrouve sur [ce site](http://labo.algo.free.fr/defi250/classement.php?id=1).
